<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $routes = Route::getRoutes();
        $arrNameRoutes = [];

        foreach ($routes as $value) {
            $arrNameRoutes[] = $value->getName();
        }

        $arrPermission = array_filter($arrNameRoutes, function ($value) {
            if (!empty($value)) {
                if (Str::of($value)->contains(['index', 'show', 'create', 'update', 'delete'])) {
                    return $value;
                }
            }
        });

        $arrPermission = array_values($arrPermission);

        foreach ($arrPermission as $permission) {
            Permission::factory()->create([
                'name' => $permission,
                'slug' => Str::slug($permission),
                'route_name' => $permission,
            ]);
        }
    }
}
