<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->name();
        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'image' => fake()->imageUrl(150, 150),
            'price' => fake()->randomNumber(6),
            'stock' => fake()->randomNumber(3),
            'descriptions' => fake()->paragraph(),
            'activated' => fake()->boolean(),
        ];
    }
}
