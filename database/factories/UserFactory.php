<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'gender' => fake()->boolean(),
            'birthday' => fake()->date('d/m/Y', 'now'),
            'avatar' => fake()->imageUrl(150, 150),
            'email' => preg_replace('/@.*/', '@deha-soft.com', fake()->safeEmail()),
            'phone' => '0975' . fake()->randomNumber(6, true),
            'password' => 'admin',
            'address' => fake()->address(),
            'activated' => fake()->boolean()
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     */
    public function unverified(): static
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
