<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Testing\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_user_if_user_exist()
    {
        $this->loginUserAdmin();
        $user = $this->createUser();
        $countRecord = User::count();
        $response = $this->deleteJson($this->getRouteDelete($user->id));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing('users', $user->toArray());
        $this->assertDatabaseCount('users', $countRecord - 1);
    }

    /** @test */
    public function authenticated_user_can_not_delete_use_if_user_not_exist()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
        $response = $this->deleteJson($this->getRouteDelete(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_user()
    {
        $user = $this->createUser();
        $response = $this->deleteJson($this->getRouteDelete($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticate_user_can_not_delete_user_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $user = $this->createUser();
        $countRecord = User::count();
        $response = $this->deleteJson($this->getRouteDelete($user->id));
        $response->assertForbidden();
        $this->assertDatabaseCount('users', $countRecord);
    }


    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteDelete($id): string
    {
        return route('users.delete', $id);
    }

    public function createUser()
    {
        return User::factory()->create();
    }
}
