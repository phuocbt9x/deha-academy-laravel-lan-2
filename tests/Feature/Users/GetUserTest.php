<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetUserTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_user()
    {
        $this->loginUserAdmin();
        $user = $this->createUser();
        $response = $this->get($this->getRouteShow($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json->has('status_code')
            ->has('html')
            ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_get_user_if_user_not_exist()
    {
        $this->loginUserAdmin();
        $response = $this->get($this->getRouteShow(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('status_code')
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_get_user_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $response = $this->get($this->getRouteShow($user->id));
        $response->assertForbidden();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteShow($id): string
    {
        return route('users.show', $id);
    }

    public function createUser()
    {
        return User::factory()->create();
    }
}
