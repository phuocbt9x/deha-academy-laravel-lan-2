<?php

namespace Tests\Feature\Users;

use App\Models\User;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_user_if_data_is_valid()
    {
        $this->loginUserAdmin();
        $user = $this->createUser();
        $dataUser = $this->makeData();
        $dataUser['avatar'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($user->id), $dataUser);
        $response->assertStatus(Response::HTTP_OK);
        $user = User::find($user->id);
        $this->assertEquals($dataUser['name'], $user->name);
    }

    /** @test */
    public function authenticated_user_can_not_update_user_if_data_not_pass_validate()
    {
        $this->loginUserAdmin();
        $user = $this->createUser();
        $dataUser = User::factory()->make([
            'name' => null,
            'email' => null,
        ])->toArray();
        unset($dataUser['email']);
        $dataUser['image'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($user->id), $dataUser);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors',
            fn(AssertableJson $json) => $json->has('name')
        )->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_user_not_exits()
    {
        $this->loginUserAdmin();
        $dataUser = $this->makeData();
        $dataUser['avatar'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate(-1), $dataUser);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_NOT_FOUND)
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_user()
    {
        $user = $this->createUser();
        $dataUser = $this->makeData();
        $dataUser['avatar'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($user->id), $dataUser);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticate_user_can_not_update_user_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $user = User::factory()->create();
        $dataUser = $this->makeData();
        $dataUser['avatar'] = $this->makeFile();
        $response = $this->putJson($this->getRouteUpdate($user->id), $dataUser);
        $response->assertForbidden();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteUpdate($id): string
    {
        return route('users.update', $id);
    }

    public function makeData(): array
    {
        $data = User::factory()->make()->toArray();
        unset($data['email']);
        return $data;
    }

    public function createUser()
    {
        return User::factory()->create();
    }

    public function makeFile(): File
    {
        Storage::disk('image');
        return UploadedFile::fake()->image(time().'.png');
    }
}
