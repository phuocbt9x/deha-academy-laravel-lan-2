<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class DeleteProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_product_if_product_exist()
    {
        $this->loginUserAdmin();
        $product = Product::factory()->create();
        $countRecord = Product::count();
        $response = $this->deleteJson($this->getRouteDelete($product->id));
        $response->assertStatus(Response::HTTP_NO_CONTENT);
        $this->assertDatabaseMissing('products', $product->toArray());
        $this->assertDatabaseCount('products', $countRecord - 1);
    }

    /** @test */
    public function authenticated_user_can_not_delete_product_if_product_not_exist()
    {
        $this->loginUserAdmin();
        $response = $this->deleteJson($this->getRouteDelete(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_product()
    {
        $product = Product::factory()->create();
        $response = $this->deleteJson($this->getRouteDelete($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticate_user_can_not_delete_product_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $product = Product::factory()->create();
        $countRecord = Product::count();
        $response = $this->deleteJson($this->getRouteDelete($product->id));
        $response->assertForbidden();
        $this->assertDatabaseCount('products', $countRecord);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteDelete($id): string
    {
        return route('products.delete', $id);
    }

    public function makeData()
    {
        return Product::factory()->create();
    }
}
