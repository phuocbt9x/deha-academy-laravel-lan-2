<?php

namespace Tests\Feature\Products;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Testing\File;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class CreateProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_create_product()
    {
        $this->loginUserAdmin();
        $countRecord = Product::count();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteCreate(), $dataProduct);
        $response->assertStatus(Response::HTTP_CREATED);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_CREATED)
            ->has('html')
            ->has('message')
            ->etc()
        );
        $this->assertDatabaseCount('products', $countRecord + 1);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_product()
    {
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteCreate(), $dataProduct);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_create_product_if_data_invalid()
    {
        $this->loginUserAdmin();
        $dataProduct = Product::factory()->make([
            'name' => null
        ])->toArray();
        $dataProduct['image'] = $dataProduct['image'] = $this->makeFile();;
        $response = $this->postJson($this->getRouteCreate(), $dataProduct);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors',
            fn(AssertableJson $json) => $json->has('name')
                ->etc()
        )
            ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_create_product_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $countRecord = Product::count();
        $dataProduct = $this->makeData();
        $dataProduct['image'] = $this->makeFile();
        $response = $this->postJson($this->getRouteCreate(), $dataProduct);
        $response->assertForbidden();
        $this->assertDatabaseCount('products', $countRecord);
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteCreate(): string
    {
        return route('products.store');
    }

    public function makeData(): array
    {
        return Product::factory()->make()->toArray();
    }

    public function makeFile(): File
    {
        Storage::disk('image');
        return UploadedFile::fake()->image(time().'.png');
    }
}
