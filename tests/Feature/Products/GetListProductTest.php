<?php

namespace Tests\Feature\Products;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetListProductTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_products()
    {
        $this->loginUserAdmin();
        $response = $this->get($this->getRouteList());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('products.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_products()
    {
        $response = $this->get($this->getRouteList());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticate_user_can_not_get_list_product_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $response = $this->get($this->getRouteList());
        $response->assertForbidden();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }

    public function getRouteList(): string
    {
        return route('products.index');
    }
}
