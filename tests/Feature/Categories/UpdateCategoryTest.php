<?php

namespace Tests\Feature\Categories;

use App\Models\Category;
use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_update_category_if_category_exists()
    {
        $this->loginUserAdmin();
        $category = $this->createCategory();
        $data = $this->makeData();
        $response = $this->putJson($this->getRouteUpdate($category->id), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_OK)
            ->has('html')
            ->has('message')
            ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_update_category()
    {
        $category = $this->createCategory();
        $data = $this->makeData();
        $response = $this->putJson($this->getRouteUpdate($category->id), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_category_not_exists()
    {
        $this->loginUserAdmin();
        $data = Category::factory()->make()->toArray();
        $response = $this->putJson($this->getRouteUpdate(-1), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(fn(AssertableJson $json) => $json->where('status_code', Response::HTTP_NOT_FOUND)
            ->has('errors')
            ->etc()
        );
    }

    /** @test */
    public function authenticated_user_can_not_update_category_if_data_is_invalid()
    {
        $this->loginUserAdmin();
        $data = Category::factory()->make([
            'name' => null
        ])->toArray();
        $category = $this->createCategory();
        $response = $this->putJson($this->getRouteUpdate($category->id), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(fn(AssertableJson $json) => $json->has('errors',
            fn(AssertableJson $json) => $json->has('name')
                ->etc()
        )
            ->etc()
        );
    }

    /** @test */
    public function authenticate_user_can_not_update_category_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $category = $this->createCategory();
        $data = $this->makeData();
        $response = $this->putJson($this->getRouteUpdate($category->id), $data);
        $response->assertForbidden();
    }

    public function getRouteUpdate($id): string
    {
        return route('categories.update', $id);
    }

    public function createCategory()
    {
        return Category::factory()->create();
    }

    public function makeData(): array
    {
        return Category::factory()->make()->toArray();
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
