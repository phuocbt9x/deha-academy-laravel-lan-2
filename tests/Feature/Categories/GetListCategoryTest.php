<?php

namespace Tests\Feature\Categories;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class GetListCategoryTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_get_list_category()
    {
        $this->loginUserAdmin();
        $response = $this->get($this->getRouteList());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('categories.index');
    }

    /** @test */
    public function unauthenticated_user_can_not_get_list_category()
    {
        $response = $this->get($this->getRouteList());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
    }

    /** @test */
    public function authenticate_user_can_not_get_list_category_if_user_has_not_permission()
    {
        $user = User::factory()->create([
            'activated' => 1,
        ]);
        $this->actingAs($user);
        $response = $this->get($this->getRouteList());
        $response->assertForbidden();
    }

    public function getRouteList(): string
    {
        return route('categories.index');
    }

    public function loginUserAdmin()
    {
        $user = User::where('email', 'admin@deha-soft.com')->first();
        $this->actingAs($user);
    }
}
