<?php

namespace Tests\Feature;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class AuthenticateTest extends TestCase
{
    /** @test */
    public function unauthenticated_user_can_see_form_register()
    {
        $response = $this->get(route('auth.register'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auths.register');
    }

    /** @test */
    public function unauthenticated_user_can_create_account_if_data_is_valid()
    {
        $data = [
            'name' => fake()->name(),
            'email' => preg_replace('/@.*/', '@deha-soft.com', fake()->safeEmail()),
            'password' => 'admin',
            'confirm_password' => 'admin'
        ];
        $countRecord = User::count();
        $response = $this->post(route('auth.reg'), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $this->assertDatabaseCount('users', $countRecord + 1);
        $response->assertRedirect(route('welcome'));
    }

    /** @test */
    public function unauthenticated_user_can_see_form_login()
    {
        $response = $this->get(route('auth.login'));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('auths.login');
    }

    /** @test */
    public function unauthenticated_user_can_login_with_account()
    {
        $user = User::factory()->create();
        $account = [
            'email' => $user->email,
            'password' => 'admin',
            'remember' => 1
        ];
        $response = $this->post(route('auth.log'), $account);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('welcome'));
    }

    /** @test */
    public function unauthenticated_user_can_not_login_with_account_if_data_is_invalid()
    {
        $account = [
            'email' => null,
            'password' => 'admin',
            'remember' => 1
        ];
        $response = $this->post(route('auth.log'), $account);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function user_can_logout()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get(route('auth.logout'));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('auth.login'));
        $this->assertGuest();
    }
}
