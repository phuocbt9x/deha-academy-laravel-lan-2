<?php

namespace App\Http\Requests\Users;

use App\Rules\PhoneRule;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:3|max:200',
            'gender' => 'required|boolean',
            'avatar' => 'sometimes',
            'birthday' => 'required|date_format:d/m/Y|before:today',
            'email' => 'missing',
            'phone' => ['required', new PhoneRule(), 'unique:users,phone,' . $this->id],
            'password' => 'nullable|min:3|max:200'
        ];
    }
}
