<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    public function sendResponse($status_code = Response::HTTP_OK, $html = null, $message = null): Response
    {
        return response()->json([
            'status_code' => $status_code,
            'html' => $html,
            'message' => $message,
        ], $status_code);
    }
}
