<?php

namespace App\Http\Controllers;

use App\Http\Requests\Categories\StoreRequest;
use App\Http\Requests\Categories\UpdateRequest;
use App\Services\CategoryService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    protected CategoryService $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        return view('categories.index');
    }

    public function list(Request $request): Response
    {
        $categories = $this->categoryService->list($request);
        $view = view('categories.paginate', compact('categories'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        $categories = $this->categoryService->create();
        $view = view('categories.create', compact('categories'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): Response
    {
        $category = $this->categoryService->store($request);
        $view = view('categories.show', compact('category'))->render();
        $message = 'The category has been created successfully.';
        return $this->sendResponse(Response::HTTP_CREATED, $view, $message);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id): Response
    {
        $category = $this->categoryService->findOrFail($id);
        $categories = $this->categoryService->create();
        $view = view('categories.update', compact(['category', 'categories']))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, $id): Response
    {
        $category = $this->categoryService->update($request, $id);
        $view = view('categories.show', compact('category'))->render();
        $message = 'The category has been updated successfully.';
        return $this->sendResponse(Response::HTTP_OK, $view, $message);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id): Response
    {
        $this->categoryService->delete($id);
        return $this->sendResponse(Response::HTTP_NO_CONTENT);
    }
}
