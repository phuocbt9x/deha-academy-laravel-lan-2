<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\StoreRequest;
use App\Http\Requests\Products\UpdateRequest;
use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class ProductController extends Controller
{
    protected ProductService $productService;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $categories = $this->productService->listCategories();
        return view('products.index', compact('categories'));
    }

    public function list(Request $request): Response
    {
        $products = $this->productService->lists($request);
        $view = view('products.paginate', compact('products'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): Response
    {
        $categories = $this->productService->listCategories();
        $view = view('products.create', compact('categories'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): Response
    {
        $product = $this->productService->store($request);
        $view = view('products.show', compact('product'))->render();
        $message = 'The product has been created successfully.';
        return $this->sendResponse(Response::HTTP_CREATED, $view, $message);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): Response
    {
        $product = $this->productService->findOrFail($id);
        $view = view('products.show', compact('product'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): Response
    {
        $product = $this->productService->findOrFail($id);
        $categories = $this->productService->listCategories();
        $view = view('products.update', compact(['categories', 'product']))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id): Response
    {
        $product = $this->productService->update($request, $id);
        $view = view('products.show', compact('product'))->render();
        $message = 'The product has been updated successfully.';
        return $this->sendResponse(Response::HTTP_OK, $view, $message);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): Response
    {
        $this->productService->delete($id);
        return $this->sendResponse(Response::HTTP_NO_CONTENT);
    }
}
