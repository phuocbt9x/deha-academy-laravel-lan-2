<?php

namespace App\Http\Controllers;

use App\Http\Requests\Users\UpdateRequest;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public UserService $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        return view('users.index');
    }

    public function list(Request $request): Response
    {
        $users = $this->userService->list($request);
        $view = view('users.paginate', compact('users'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id): Response
    {
        $user = $this->userService->findOrFail($id);
        $view = view('users.show', compact('user'))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id): Response
    {
        $user = $this->userService->findOrFail($id);
        $roles = $this->userService->listRoles();
        $view = view('users.update', compact(['user', 'roles']))->render();
        return $this->sendResponse(Response::HTTP_OK, $view);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, string $id): Response
    {
        $user = $this->userService->update($request, $id);
        $view = view('users.show', compact('user'))->render();
        $message = 'The user has been updated successfully.';
        return $this->sendResponse(Response::HTTP_OK, $view, $message);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): Response
    {
        $this->userService->delete($id);
        return $this->sendResponse(Response::HTTP_NO_CONTENT);
    }
}
