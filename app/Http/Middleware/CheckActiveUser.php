<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckActiveUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!Auth::check()){
            return redirect()->route('auth.login')->withErrors([
                'warning' => 'You need to log in to continue.',
            ]);
        }

        if (empty(Auth::user()->activated)) {
            Auth::logout();
            return redirect()->route('auth.login')->withErrors([
                'errorLogin' => 'Your account has not been activated.',
            ]);
        }
        return $next($request);
    }
}
