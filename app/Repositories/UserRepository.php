<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{
    public function model(): string
    {
        return User::class;
    }

    public function search($name)
    {
        return $this->model->orWithName($name)->orWithEmail($name)
            ->latest('id')->paginate(config('app.paginate.limit', 10))->withQueryString();
    }

    public function findEmail($email)
    {
        return $this->model->orWithEmail($email)->first();
    }
}
