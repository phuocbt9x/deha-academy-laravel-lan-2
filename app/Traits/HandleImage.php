<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

trait HandleImage
{
    const QUALITY_IMG = 80;
    public string $path;
    public string $disk = 'image';

    public function verify($image): bool
    {
        return !empty($image);
    }

    public function saveImage($image): string|null
    {
        if ($this->verify($image)) {
            $extension = $image->getClientOriginalExtension();
            $image = Image::make($image)->encode($extension, self::QUALITY_IMG);
            $name = time().'.'.$extension;
            $pathImage = $this->path.$name;
            Storage::disk($this->disk)->put($pathImage, $image);
            return $name;
        }
        return null;
    }

    public function updateImage($image, $currentImage): string|null
    {
        if ($this->verify($image)) {
            $this->deleteImage($currentImage);
            return $this->saveImage($image);
        }
        return $currentImage;
    }

    public function deleteImage($name): void
    {
        if ($this->exitsImage($name) && !empty($name)) {
            Storage::disk($this->disk)->delete($this->getImagePath($name));
        }
    }

    public function exitsImage($name): bool
    {
        return Storage::disk($this->disk)->exists($this->getImagePath($name));
    }

    public function getImagePath($name): string
    {
        return $this->path . $name;
    }

    public function getImageFullPath($name): string
    {
        if ($this->verify($name)) {
            return Str::of($name)->isMatch('/http(.*)/') ? $name : asset($this->getImagePath($name));
        }
        return asset(config('app.image.default'));
    }

    /**
     * @param  string  $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path . '/';
    }

    /**
     * @param  string  $disk
     */
    public function setDisk(string $disk): void
    {
        $this->disk = $disk;
    }
}
