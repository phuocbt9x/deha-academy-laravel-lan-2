<?php
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function (){
    Route::controller(UserController::class)->group(function (){
        Route::prefix('users')->group(function (){
            Route::as('users.')->group(function (){
                Route::get('/', 'index')->name('index')
                    ->middleware('hasPermission:users,users.index');
                Route::get('/list', 'list')->name('list')
                    ->middleware('hasPermission:users,users.index');
                Route::get('/show/{id}', 'show')->name('show')
                    ->middleware('hasPermission:users,users.show');
                Route::get('edit/{id}', 'edit')->name('edit')
                    ->middleware('hasPermission:users,users.update');
                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('hasPermission:users,users.update');
                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('hasPermission:users,users.delete');
            });
        });
    });
});
