<?php

use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function () {
    Route::controller(RoleController::class)->group(function () {
        Route::prefix('roles')->group(function () {
            Route::as('roles.')->group(function () {
                Route::get('/', 'index')->name('index')
                    ->middleware('hasPermission:roles,roles.index');
                Route::get('/list', 'list')->name('list')
                    ->middleware('hasPermission:roles,roles.index');
                Route::get('/create', 'create')->name('create')
                    ->middleware('hasPermission:roles,roles.update');
                Route::post('/store', 'store')->name('store')
                    ->middleware('hasPermission:roles,roles.create');
                Route::get('edit/{id}', 'edit')->name('edit')
                    ->middleware('hasPermission:roles,roles.update');
                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('hasPermission:roles,roles.update');
                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('hasPermission:roles,roles.delete');
            });
        });
    });
});
