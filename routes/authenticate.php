<?php

use App\Http\Controllers\AuthenticateController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthenticateController::class)->group(function (){
    Route::as('auth.')->group(function (){
        Route::get('login', 'viewLogin')->name('login');
        Route::post('log', 'login')->name('log');
        Route::get('register', 'viewRegister')->name('register');
        Route::post('reg', 'register')->name('reg');
        Route::get('logout', 'logout')->name('logout');
        //social
        Route::get('login/social/{provider}', 'socialRedirect')->name('social.redirect');
        Route::get('login/social/{provider}/callback', 'socialCallback')->name('social.callback');
    });
});
