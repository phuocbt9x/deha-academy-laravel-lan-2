<?php

use App\Http\Controllers\CategoryController;
use Illuminate\Support\Facades\Route;

Route::middleware('checkActiveUser')->group(function () {
    Route::controller(CategoryController::class)->group(function () {
        Route::prefix('categories')->group(function () {
            Route::as('categories.')->group(function () {
                Route::get('/', 'index')->name('index')
                    ->middleware('hasPermission:categories,categories.index');
                Route::get('/list', 'list')->name('list')
                    ->middleware('hasPermission:categories,categories.index');
                Route::get('/create', 'create')->name('create')
                    ->middleware('hasPermission:categories,categories.create');
                Route::post('/store', 'store')->name('store')
                    ->middleware('hasPermission:categories,categories.create');
                Route::get('edit/{id}', 'edit')->name('edit')
                    ->middleware('hasPermission:categories,categories.update');
                Route::put('update/{id}', 'update')->name('update')
                    ->middleware('hasPermission:categories,categories.update');
                Route::delete('delete/{id}', 'destroy')->name('delete')
                    ->middleware('hasPermission:categories,categories.delete');
            });
        });
    });
});

