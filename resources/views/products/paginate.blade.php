@php use App\Helpers\Helper; @endphp
<table class="table table-hover text-nowrap">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Image</th>
        <th>Price</th>
        <th>Stock</th>
        <th>Status</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if($products->isNotEmpty())
        @foreach($products as $product)
            <tr>
                <td>
                    {{ $product->id }}
                </td>
                <td>
                    {{ $product->name }}
                </td>
                <td>
                    <img src="{{ $product->image }}" alt=""
                         style="object-fit: cover; width: 110px; height: 110px;"
                         class="img-thumbnail img-circle">
                </td>
                <td>
                    {{ number_format($product->price) }}
                </td>
                <td>
                    {{ number_format($product->stock) }}
                </td>
                <td>
                    {!! Helper::getStatus($product->activated) !!}
                </td>
                <td>
                    @hasPermission('products', 'products.show')
                    <a href="{{ route('products.show', $product->id) }}"
                       class="btn btn-sm btn-info btn-open-modal" data-toggle="modal">
                        <i class="fas fa-eye"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('products', 'products.update')
                    <a href="{{ route('products.edit', $product->id) }}"
                       class="btn btn-sm btn-secondary btn-open-modal"
                       data-toggle="modal">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('products', 'products.delete')
                    <a href="{{ route('products.delete', $product->id) }}"
                       class="btn btn-sm btn-danger btn-delete">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7" class="text-center">
                <span style="color: red">No data found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="mt-3 mr-2 ml-2">
    {{ $products->links() }}
</div>
