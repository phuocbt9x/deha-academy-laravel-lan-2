@php use App\Helpers\Helper; @endphp
<table class="table table-hover text-nowrap">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Image</th>
        <th>Gender</th>
        <th>Email</th>
        <th>Status</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if($users->isNotEmpty())
        @foreach($users as $user)
            <tr>
                <td>
                    {{ $user->id }}
                </td>
                <td>
                    {{ $user->name }}
                </td>
                <td>
                    <img src="{{ $user->avatar }}" alt=""
                         style="width:110px; height:110px; object-fit: cover;"
                         class="img-thumbnail img-circle">
                </td>
                <td>
                    {{ Helper::getGender($user->gender) }}
                </td>
                <td>
                    {{ $user->email }}
                </td>
                <td>
                    {!! Helper::getStatus($user->activated) !!}
                </td>
                <td>
                    @hasPermission('users', 'users.show')
                    <a href="{{ route('users.show', $user->id) }}"
                       class="btn btn-sm btn-info btn-open-modal" data-toggle="modal">
                        <i class="fas fa-eye"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('users', 'users.update')
                    <a href="{{ route('users.edit', $user->id) }}"
                       class="btn btn-sm btn-secondary btn-open-modal" data-toggle="modal">
                        <i class="fas fa-edit"></i>
                    </a>
                    @endhasPermission

                    @hasPermission('users', 'users.delete')
                    <a href="{{ route('users.delete', $user->id) }}"
                       class="btn btn-sm btn-danger btn-delete">
                        <i class="fas fa-trash"></i>
                    </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="7" class="text-center">
                <span style="color: red">No data found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="mt-3 mr-2 ml-2">
    {{ $users->links() }}
</div>
