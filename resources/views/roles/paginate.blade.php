@php use App\Helpers\Helper; @endphp
<table class="table table-hover text-nowrap">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Slug</th>
        <th>Permission</th>
        <th>Status</th>
        <th>Action(s)</th>
    </tr>
    </thead>
    <tbody>
    @if($roles->isNotEmpty())
        @foreach($roles as $role)
            <tr>
                <td>
                    {{ $role->id }}
                </td>
                <td>
                    {{ $role->name }}
                </td>
                <td>
                    {{ $role->slug }}
                </td>
                <td>
                    @foreach($role->permissions as $permission)
                        {!! $permission->name . '<br>'!!}
                    @endforeach
                </td>
                <td>
                    {!! Helper::getStatus($role->activated) !!}
                </td>
                <td>
                    @hasPermission('roles', 'roles.update')
                        <a href="{{ route('roles.edit', $role->id) }}"
                           class="btn btn-sm btn-secondary btn-open-modal" data-toggle="modal">
                            <i class="fas fa-edit"></i>
                        </a>
                    @endhasPermission

                    @hasPermission('roles', 'roles.delete')
                        <a href="{{ route('roles.delete', $role->id) }}"
                           class="btn btn-sm btn-danger btn-delete">
                            <i class="fas fa-trash"></i>
                        </a>
                    @endhasPermission
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="6" class="text-center">
                <span style="color: red">No data found.</span>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<div class="mt-3 mr-2 ml-2">
    {{ $roles->links() }}
</div>
