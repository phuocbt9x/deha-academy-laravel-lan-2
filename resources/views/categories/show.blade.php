@php use App\Helpers\Helper; @endphp
<div class="modal fade" id="modal-form">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">About Category</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <strong>Name</strong>

                                <p class="text-muted">
                                    {{ $category->name }}
                                </p>

                                <hr>

                                <strong>Slug</strong>

                                <p class="text-muted">
                                    {{ $category->slug }}
                                </p>

                                <hr>
                                @if($category->parentCategory->exists)
                                    <strong>Parent</strong>

                                    <p class="text-muted">
                                        {{ $category->parentCategory->name }}
                                    </p>

                                    <hr>
                                @endif

                                @if($category->subCategories->isNotEmpty())
                                    <strong>SubCategories</strong>

                                    <p class="text-muted">
                                        @foreach($category->subCategories as $sub)
                                            <span class="tag tag-danger">{{ $sub->name }}</span>
                                            <br>
                                        @endforeach
                                    </p>
                                @endif
                                <strong>Status</strong>

                                <p class="text-muted">
                                    {!! Helper::getStatus($category->activated) !!}
                                </p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <!--/.col (left) -->
                </div>
            </div>
            <div class="modal-footer float-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

